  
  
__Astrology website__

The website was made as the part of final project at the
online course "CS50 Introduction to Computer Science" 
taught by HarvardX and Edx 

Languages and technologies used: PHP, JS, HTML, CSS, SQL, JQuery, AJAX

Currently accessible at **www.astrologyadvice.ru**


__  

_Alexander S. Yakovlev_
