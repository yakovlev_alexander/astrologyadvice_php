<!DOCTYPE html>

<html>

    <head>
            
        <meta name="yandex-verification" content="9edea45a9c8cfd15" />
        <meta name="google-site-verification" content="4v7AJyBmrpbP_V6Bo4butGIVebXj54gW3mRXSWaxLfQ" />
            <meta charset="utf-8">
            <link href="css/bootstrap.min.css" rel="stylesheet"/>
            <link href="css/bootstrap-theme.min.css" rel="stylesheet"/>
            <link href="css/styles.css" rel="stylesheet"/>
            <!--<link rel="stylesheet" href="static/normalize.css">-->
<!--            <link rel="stylesheet" href="static/main.css">-->
            <!--<link rel="stylesheet" href="static/jquery.jgrowl.css">-->
            <meta name="robots" content="noindex,nofollow"/>
        <link rel="icon" type="image/ico" href="http://www.astrologyadvice.ru/favicon.ico" />
        <?php if (isset($title)): ?>
            <title>Astrology advice: <?= htmlspecialchars($title) ?></title>
        <?php else: ?>
            <title>Astrology advice</title>
        <?php endif ?>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/drop_articles.js"></script>
<!--        <script src="js/make_order.js"></script>-->
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?122"></script>
        <script type="text/javascript">
          VK.init({apiId: 5201241, onlyWidgets: true});
        </script>
        <!-- Put this script tag to the <head> of your page -->
        <!--Share-->
        <script type="text/javascript" src="http://vk.com/js/api/share.js?93" charset="windows-1251"></script>
<!--Like-->
<!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?126"></script>

<script type="text/javascript">
  VK.init({apiId: 5201241, onlyWidgets: true});
</script>

        <!--LiveInternet counter-->   
        <script type="text/javascript"><!— 
            document.write("<a href='//www.liveinternet.ru/click' "+ 
            "target=_blank><img src='//counter.yadro.ru/hit?t26.6;r"+ 
            escape(document.referrer)+((typeof(screen)=="undefined")?"": 
            ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth? 
            screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+ 
            ";"+Math.random()+ 
            "' alt='' title='LiveInternet: показано число посетителей за"+ 
            " сегодня' "+ 
            "border='0' width='88' height='15'><\/a>")//-->
        </script>
        <!--/LiveInternet--> 

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter38785670 = new Ya.Metrika({
                    id:38785670,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38785670" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

    </head>

    <body>

        <div class="container">
            <div class="lang">
                <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-default" id="rusBtn" 
                            onclick="switchRusLanguage()">Русский</button>
                    <button type="button" class="btn btn-default" id="engBtn" 
                            onclick="switchEngLanguage()">English</button>
                </div>


            </div> 
                <div id="top">
                    <a href="/"><img alt="Astrology advice" src="img/logo1.png"/></a>
                </div>
            <hr>