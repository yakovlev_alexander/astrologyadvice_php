<div>    
    <div class = "left_col">
        <ul  class = "nav nav-pills nav-stacked">
            
            <?php if ("{$_SESSION["lang"]}" === "rus"): ?>
            <li><a href="lovehoroscope">Любовный гороскоп</a></li>
            <li><a href="compatibility">Гороскоп совместимости</a></li>
            <li><a href="destiny">Индивидуальный гороскоп судьбы</a></li>
            <li><a href="karma">Кармический гороскоп</a></li>
            <li><a href="profession">Гороскоп профессии и финансов</a></li>
            <li><a href="children">Детский гороскоп</a></li>
            <li><a href="timeofbirth">Восстановление времени рождения</a></li>
            <li><a href="importantevent">Подбор даты важного события</a></li>
            <li><a href="consultation">Консультация астролога</a></li>
            <li><a href="personal">Персональный астрологический прогноз</a></li>

            <?php elseif ("{$_SESSION["lang"]}" === "eng"): ?>
            <li><a href="lovehoroscope">Love horoscope</a></li>
            <li><a href="compatibility">Compatibility horoscope</a></li>
            <li><a href="destiny">Individual horoscope of destiny</a></li>
            <li><a href="karma">Karma horoscope</a></li>
            <li><a href="profession">Horoscope of profession and finance</a></li>
            <li><a href="children">Children's horoscope</a></li>
            <li><a href="timeofbirth">Recovery the time of birth</a></li>
            <li><a href="importantevent">Important event</a></li>
            <li><a href="consultation">Consultation of the astrologer</a></li>
            <li><a href="personal">Personal astrology forecast</a></li>
            <?php endif ?>
        </ul>
    </div>

    <div class ="right_col">

    </div>
    
</div>



