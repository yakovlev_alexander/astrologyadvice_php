<div id = "page_content">
    <div class = "left_col">       
        <ul  class = "nav nav-pills nav-stacked">
            <?php if ("{$_SESSION["lang"]}" === "rus"): ?>
            <li><a href="about">О себе...</a></li>
            <li><a href="whyAstrology">Почему астрология</a></li>
            <li><a href="services">Виды консультаций и цены</a></li>
            <li><a href="order">Сделать заказ</a></li>
            
            <?php elseif ("{$_SESSION["lang"]}" === "eng"): ?>
            <li><a href="about">About me...</a></li>
            <li><a href="whyAstrology">Why Astrology?</a></li>
            <li><a href="services">Services provided</a></li>
            <li><a href="order">Make an order</a></li>
            <?php endif ?>
        </ul>
    </div>
    
    <div class ="right_col">
        
    </div>    
   
</div>

