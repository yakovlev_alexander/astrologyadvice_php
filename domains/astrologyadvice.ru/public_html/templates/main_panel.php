<div id = "main_panel" class="main_panel">  
    <ul class="nav nav-pills"> 

        <?php if ("{$_SESSION["lang"]}" === "rus"): ?>
        <li><a href="index.php">Главная страница</a></li>
        <li><a href="service.php">Виды консультаций</a></li>
        <li><a href="articles.php">Статьи по астрологии</a></li>
        <li><a href="contacts.php">Контакты</a></li>
        <li><a href="feedback.php">Отзывы и обратная связь</a></li>
        
        <?php elseif ("{$_SESSION["lang"]}" === "eng"): ?>
        <li><a href="index.php">Main page</a></li>
        <li><a href="service.php">Service (detailed)</a></li>
        <li><a href="articles.php">Astrology articles</a></li>
        <li><a href="contacts.php">Contacts</a></li>
        <li><a href="feedback.php">Feedback</a></li>
        <?php endif ?>
    </ul>
</div>
    <hr>
<div id="middle">
