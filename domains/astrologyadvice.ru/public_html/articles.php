<?php
    require("include/config.php");
    
    $rows = query("SELECT id, artname, arttext, DATE(date) AS date, filename "
            . "FROM astroart ORDER BY date");
   
    $articles = [];
    foreach ($rows as $row)
    {
            $articles[] = [
                "id" => $row["id"],
                "artname" => $row["artname"],
                "arttext" => $row["arttext"],
                "date" => $row["date"],
                "filename" => $row["filename"]
            ];
    }
        // render articles
    render("articles_form.php", ["articles" => $articles]);


?>

