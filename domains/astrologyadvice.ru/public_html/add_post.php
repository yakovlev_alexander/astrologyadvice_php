<?php

require("include/config.php");

if(!isset($_SESSION['access']) || $_SESSION['access']!=true){ // Если не введен пароль
    header("location: articles.php");
    
}
else{
// if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        // render add_post_form
        render("add_post_form.php", []);
    }
    
    // else if user reached page via POST (as by submitting a form via POST)
    elseif ($_SERVER["REQUEST_METHOD"] == "POST")
    {   
        //print_r($_POST); Если раскомментить не будет работать redirect(""), т.к. содержит ф-ю header
//////////////////////Загрузка файла на сервер
//            [name] => MyFile.txt (comes from the browser, so treat as tainted)
//            [type] => text/plain  (not sure where it gets this from - assume the browser, so treat as tainted)
//            [tmp_name] => /tmp/php/php1h4j1o (could be anywhere on your system, depending on your config settings, but the user has no control, so this isn't tainted)
//            [error] => UPLOAD_ERR_OK  (= 0)
//            [size] => 123   (the size in bytes)
try {
//    apologize(var_dump($_FILES));

    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
    if (
        !isset($_FILES['upfile']['error']) ||
        is_array($_FILES['upfile']['error'])
    ) {
        throw new RuntimeException('Invalid parameters.');
    }

    // Check $_FILES['upfile']['error'] value.
    switch ($_FILES['upfile']['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
        default:
            throw new RuntimeException('Unknown errors.');
    }

    // You should also check filesize here. 
    if ($_FILES['upfile']['size'] > 512 * 1024) {
        throw new RuntimeException('Exceeded filesize limit.');
    }

    // DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
    // Check MIME Type by yourself.
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    if (false === $ext = array_search(
        $finfo->file($_FILES['upfile']['tmp_name']),
        array(
            'jpg' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif',
        ),
        true
    )) {
        throw new RuntimeException('Invalid file format.');
    }
    $filename = sprintf('%s.%s',sha1_file($_FILES['upfile']['tmp_name']),$ext);
    // You should name it uniquely.
    // DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
    // On this example, obtain safe unique name from its binary data.
    if (!move_uploaded_file(
        $_FILES['upfile']['tmp_name'],
        sprintf('files/%s.%s',
            sha1_file($_FILES['upfile']['tmp_name']),
            $ext
        )
    )) {
        throw new RuntimeException('Failed to move uploaded file.');
    }

    echo 'File is uploaded successfully.';

} catch (RuntimeException $e) {

    echo $e->getMessage();

}
/////////
        
        if (empty($_POST["InputArtName"]))
        {
            apologize("Не указано название статьи");          
        }
        else if (empty($_POST["InputArtText"]))
        {
            apologize("Не введен текст статьи");
        }
        else
        {
            query("INSERT INTO astroart(artname, arttext, filename, date) VALUES(?, ?, ?, CURRENT_TIMESTAMP)",
                $_POST["InputArtName"], $_POST["InputArtText"],$filename);                                                    
        }
        //    redirect("");
    }
}
?>



