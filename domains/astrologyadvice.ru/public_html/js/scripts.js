/**
 * scripts.js
 * Global JavaScript, if any.
 */
//Переключение языков
var rusBtn = document.getElementById("rusBtn"),
    engBtn = document.getElementById("engBtn"),
    lang;
function switchRusLanguage() {
    location.href="index.php?lang=rus";    
};
function switchEngLanguage() {
    location.href="index.php?lang=eng";
};

/////////////////////Для main page form и service form
var     links,
        updateState,
        updateButtons,
        rightMainEl,
        leftMenuEl,
        mainMenuEl,
        makeOrder;

// Присвоение объектов - область текста с разделителем экрана на две части
rightMainEl = document.querySelector('.right_col');
leftMenuEl = document.querySelector('.left_col');
mainMenuEl = document.querySelector('.main_panel');

//makeOrder = document.getElementsByName('makeOrder')[0];
//console.log(makeOrder);
//линки для вызова текста
links = {
    whyAstrology: "text/main_why_rus.html",
    services: "text/main_services_rus.html",
    about: "text/main_about_rus.html",
    order: "text/main_order.html",
    
    lovehoroscope: "text/services_love.html",
    compatibility: "text/services_compatibility.html",
    destiny: "text/services_destiny.html",
    karma: "text/services_karma.html",
    profession: "text/services_profession.html",
    children: "text/services_children.html",
    timeofbirth: "text/services_timeofbirth.html",
    importantevent: "text/services_importantevent.html",
    consultation: "text/services_consultation.html",
    personal: "text/services_personal.html"
};

//Вариант с hash'ом
//updateState = function(){
//    var content = links[location.hash.slice(1)]; //Remove hash
//    rightMainEl.innerHTML = content || "The page is not ready!";
////    elemClicked.className = "list-group-item active";
//};
//Функция загрузки текста в правую текстовую область
updateState = function(state){
    if (!state) return;         // нет аргумента
    console.log(state);
    $(function(){
      $(".right_col").load(links[state.page]); 
    });
//    console.log($(".right_col").load(links[state.page]));
    //rightMainEl.innerHTML = links[state.page]; 
    updateButtons(state);
};
//Функция активной кнопки левой панели
updateButtons = function(state){
    [].slice.call(leftMenuEl.querySelectorAll('a')).forEach(function(e){
        var classList = e.parentNode.classList;
        state.page === e.getAttribute('href')
            ? classList.add('active')
            : classList.remove('active');
    });
    [].slice.call(mainMenuEl.querySelectorAll('a')).forEach(function(e){
        var classList = e.parentNode.classList;
        state.page === e.getAttribute('href')
            ? classList.add('active')
            : classList.remove('active');
    });
};

//window.addEventListener('hashchange',updateState);
//window.addEventListener('load',updateState);
//лисинер на перемещение вперед/назад
window.addEventListener('popstate', function (e) {
    updateState(e.state);
});
//Лисинер на клик по кнопке левого меню
leftMenuEl.addEventListener('click', function (e){
    var state; 
//    console.log(e.target.getAttribute('href'));
    if (e.target.tagName !== 'A') return; //разделитель <a></a>
    state = {
        page: e.target.getAttribute('href')
    };
//    console.log(state);
    history.pushState(state,'',state.page);
    updateState(state);
    e.preventDefault();
});

rightMainEl.addEventListener('click', function (e){
    var state; 
    //console.log(e.target.getAttribute('name'));
    if (e.target.tagName !== 'A') return; //разделитель <a></a>
    switch (e.target.getAttribute('name')) {
        case 'sample_love': 
            window.open('/pdf/sample_love.pdf');
            break;
        case 'sample_compat': 
            window.open('/pdf/sample_compat.pdf');
            break;
        case 'sample_finance': 
            window.open('/pdf/sample_finance.pdf');
            break;
        case 'sample_child': 
            window.open('/pdf/sample_child.pdf');
            break;
        case 'sample_individual': 
            window.open('/pdf/sample_individual.pdf');
            break;
        case 'sample_karma': 
            window.open('/pdf/sample_karma.pdf');
            break;
        case 'make_order': 
            state = {
                page: 'order'
            };
            history.pushState(state,'',state.page);
            updateState(state);
            e.preventDefault();
            break; 
    }
//    else if (e.target.getAttribute('name') == 'sample_love'){
//        window.open('/pdf/sample_love.pdf')
//    }
//    else if (e.target.getAttribute('name') == 'make_order'){
//        state = {
//            page: 'order'
//        };
//    //    console.log(state);
//        history.pushState(state,'',state.page);
//    //      window.window.location.href = "index.php"
//        updateState(state);
//        e.preventDefault();
//    }
});

    function pageLoad(){ 
        if (window.location["pathname"] == "/index.php" || window.location["pathname"] == "/")
            $(function(){
                $(".right_col").load(links["about"]); 
            });
        else if (window.location["pathname"] == "/service.php")
            $(function(){
                $(".right_col").load(links["lovehoroscope"]); 
            });
        console.log(window.location["pathname"]);
    }
window.onload = pageLoad();
/////////////////////Для main page form и service form

//$(.main_panel).click(function(){
//    if
//});