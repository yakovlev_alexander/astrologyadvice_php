/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready (function() {
    $("#submitOrder").click(function (){
        console.log($("#horoscopeTypeOrder").val());
        $("#messageShow").hide();
        var name = $("#inputNameOrder").val();
        var email = $("#inputEmailOrder").val();
        var text = $("#textOrder").val();
        var horoscopeType = $("#horoscopeTypeOrder").val();
        var fail = "";
        if (name.length < 2) fail = "Имя должно быть не меньше 2 символов";
        else if (email.split('@').length - 1 === 0 || email.split('.').length - 1 === 0)
                fail = "Введен не корректный email";
        else if (horoscopeType.toString() == "Другое" && text.length < 10)
                fail = "Если тип услуги выбран \"Другое\", пожалуйста напишите ваш вопрос";
        if (fail !== ""){
            $('#messageShow').html(fail + "<div class='clear'><br></div>");
            $('#messageShow').show();
            return false;
        } 
        $.ajax({
           url: 'index.php',
           type: 'POST',
           cache: false,
           data: {'name': name, 'email': email, 'text': text, 'horoscopeType': horoscopeType},
           dataType: 'html',
           success: function(data){
                $('#messageShow').html(data + "<div class='clear'><br></div>");
                $('#messageShow').show();
           }
        });
            
    });    
});
